import { PushNotificationService } from './push-notification.service';
import { Controller, Post, Body } from '@nestjs/common';

@Controller('push-notification')
export class PushNotificationController {
  constructor(
    private readonly pushNotificationService: PushNotificationService,
  ) {}

  @Post()
  async sendPushNotification(
    @Body() body: { token: string; title: string; message: string },
  ) {
    const { token, title, message } = body;
    await this.pushNotificationService.sendNotification(token, {
      title,
      body: message,
    });
  }
}
