import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PushNotificationService } from './push-notification.service';
import { PushNotificationController } from './push-notification.controller';
import * as admin from 'firebase-admin';

@Module({
  controllers: [PushNotificationController],
  providers: [
    PushNotificationService,
    {
      provide: 'FIREBASE_MESSAGING_PROVIDER',
      useFactory: (configService: ConfigService) => {
        const app = admin.initializeApp({
          credential: admin.credential.cert({
            projectId: configService.get('FIREBASE_PROJECT_ID'),
            clientEmail: configService.get('FIREBASE_CLIENT_EMAIL'),
            privateKey: configService
              .get('FIREBASE_PRIVATE_KEY')
              .replace(/\\n/g, '\n'),
          }),
          // databaseURL: configService.get('FIREBASE_DATABASE_URL'),
        });
        return app.messaging();
      },
      inject: [ConfigService],
    },
  ],
  exports: ['FIREBASE_MESSAGING_PROVIDER'],
  imports: [ConfigModule.forRoot()],
})
export class PushNotificationModule {}
