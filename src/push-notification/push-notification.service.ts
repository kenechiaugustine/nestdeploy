import { Injectable, Inject } from '@nestjs/common';
import { messaging } from 'firebase-admin';

@Injectable()
export class PushNotificationService {
  constructor(
    @Inject('FIREBASE_MESSAGING_PROVIDER')
    private readonly messaging: messaging.Messaging,
  ) {}

  async sendNotification(token: string, data: any) {
    await this.messaging.send({
      token,
      notification: {
        title: data.title,
        body: data.body,
      },
    });
  }
}
