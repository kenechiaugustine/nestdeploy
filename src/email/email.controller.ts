import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';

import { EmailService } from './email.service';

@Controller('email')
export class EmailController {
  constructor(private readonly emailService: EmailService) {}

  @Get()
  getUsers() {
    return this.emailService.sendEmail(
      'kenechiaugustine@gmail.com',
      'Welcome to this app',
      'hello',
      {
        firstName: 'Kenechukwu',
        lastName: 'Arionye',
      },
    );
  }
}
