import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class EmailService {
  constructor(private mailerService: MailerService) {}

  async sendEmail(
    email: string,
    subject: string,
    template: string,
    context: {},
  ) {
    await this.mailerService.sendMail({
      to: email,
      from: process.env.MAIL_FROM,
      subject,
      template,
      context,
    });
  }
}
