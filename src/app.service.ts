import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import * as FIREBASE_CREDENTIAL from '../firebase-cred.json';
import { EmailService } from './email/email.service';

@Injectable()
export class AppService {
  constructor(private readonly mailService: EmailService) {}

  async getHello() {
    return {
      message: 'Hello world',
    };
  }

  async sendEmail() {
    // Send Email
    await this.mailService.sendEmail(
      `kenechiaugustine@gmail.com`,
      'Email sending subject',
      'hello',
      {
        firstName: 'Augustine',
      },
    );

    return {
      message: '✅ Email sent',
    };
  }
}
