import { Injectable, BadRequestException } from '@nestjs/common';
import { Twilio } from 'twilio';

@Injectable()
export class SmsService {
  private twilioClient;

  constructor() {
    const accountSid = process.env.TWILIO_ACCOUNT_SID;
    const authToken = process.env.TWILIO_AUTH_TOKEN;

    this.twilioClient = new Twilio(accountSid, authToken);
  }

  /******************************************
   ********** Custom SMS Sending **********
   ******************************************/

  async sendSmsMessage(receiverPhoneNumber: string, message: string) {
    try {
      const result = await this.twilioClient.messages.create({
        body: message,
        from: process.env.TWILIO_SENDER_PHONE_NUMBER,
        to: receiverPhoneNumber,
      });

      console.log(`🚀 SMS sent to ${receiverPhoneNumber} \n ${result}`);
    } catch (e) {
      console.log(`🔥 Error sending sms to ${receiverPhoneNumber} \n ${e}`);
    }

    return;
  }

  /******************************************
   ***** Custom twilio verification *********
   ******************************************/
  initiatePhoneNumberVerificationBySms(phoneNumber: string) {
    return this.twilioClient.verify.v2
      .services(process.env.TWILIO_PHONE_SMS_VERIFICATION_SERVICE_SID)
      .verifications.create({ to: phoneNumber, channel: 'sms' });
  }

  async confirmPhoneNumberVerificationBySms(
    phoneNumber: string,
    verificationCode: string,
  ) {
    const result = await this.twilioClient.verify.v2
      .services(process.env.TWILIO_PHONE_SMS_VERIFICATION_SERVICE_SID)
      .verificationChecks.create({ to: phoneNumber, code: verificationCode });

    if (!result.valid || result.status !== 'approved') {
      throw new BadRequestException('Wrong code provided');
    }

    return result;
  }
}
