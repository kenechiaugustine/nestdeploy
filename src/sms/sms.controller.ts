import { Controller, Post } from '@nestjs/common';
import { SmsService } from './sms.service';

@Controller('sms')
export class SmsController {
  constructor(private readonly smsService: SmsService) {}

  @Post('msg')
  async sendDefaultMessage() {
    await this.smsService.sendSmsMessage(
      '+234807904489390',
      'Hello bro, Sup?',
    );
  }

  @Post('init')
  async initiatePhoneNumberVerification() {
    await this.smsService.initiatePhoneNumberVerificationBySms(
      '+23408079044893',
    );
  }

  @Post('check')
  async checkVerificationCode() {
    await this.smsService.confirmPhoneNumberVerificationBySms(
      '+2348079044893',
      '089844',
    );
  }
}
